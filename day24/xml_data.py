import xml.etree.ElementTree as ET

tree = ET.parse('data.xml')
root = tree.getroot()

for key in root:
    print(dir(key))
    print(key.tag, key.attrib)
