# with
import csv
# k = open('demo.txt', 'r')
# k.read()
#
# k.close()

class file_open():
    def __init__(self):
        pass

    def __enter__(self):
        print('enter has compiled')

    def __exit__(self, exc_type, exc_value, exc_traceback):
        print('exit has compiled')

with file_open() as file:
    print("i am in between")

with open('demo.csv') as file:
    import pdb
    pdb.set_trace()
    h = csv.reader(file)
    next(h)
    for each in h:
        print(each)


