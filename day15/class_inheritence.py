# # explict calling

# class first():
#     # def __init__(self):
#     #     pass
#     def hi(self):
#         print("i am first")
#
# class second():
#     def bye(self):
#         print("i am second")


# k_f = first()
# k_f.hi()
# k_s = second()
# k_s.bye()

# # inheritance
#
# class first():
#     j =2
#     # def __init__(self):
#     #     pass
#     def hi(self):
#         print("i am first")
#
# class second(first, ):
#     k = 1
#     def bye(self):
#         print("i am second")
#
#
#
# k_s = second()
# k_s.bye()
# k_s.hi()

# method overriding

# class first():
#     j =2
#     # def __init__(self):
#     #     pass
#     def hi(self,):
#         print("i am first")
#
#
# class second(first ):
#     k = 1
#     def bye(self):
#         print("i am second")
#
#     def hi(self):
#         print("this is overrided")
#
#
# k_s = second()
# k_s.bye()
# k_s.hi()

# using super

class first():
    def __init__(self):
        self.name2 = "kuna"
    def hi(self):
        print(self.name2)

#
class second(first):
    def __init__(self):
        super().__init__()
        self.name = "rao"
    def bye(self):
        print(self.name)



k_s = second()
k_s.bye()
k_s.hi()