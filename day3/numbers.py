###Add,  power
##
##k = 1 + 2
##print('numerical',k)
##
###string
##
##k = "hi" + "bye"
##print('string',k)
##
###mul
##
##k = 2 * 3
##print('num',k)
##
###string
##k = 'h' * 3
##print('str',k)
##
###power
##k = 2 ** 3
##print(k)

###random
##import random
##print(random.random())
##print(dir(random))


###eval
##k = "2 ** 3"
##print(k)
##print(eval(k))

###quotes
##k = " my bro's name is kuna "
##print(k)

#TODO
#string formating

###assignment
##a = b = 1
##print(a)
##
##a, b = 1, 2
##print(a, b)
##
###swap
##b,a = a,b
##
##print(a, b)

###memory managment
##
##a = 1
##
##b = a
##
##print(hex(id(a)))
##print(hex(id(b)))
##print('b is', b)
##
##a = 2
##
##print('a is', a)
##print('b is', b)
##
##print(hex(id(a)))

import copy

a = [1,2,3]
b = a
a[0] = 4
print(a)
print(b)

a = [1,2,3]
b = copy.copy(a)
a[0] = 5
print(a)
print(b)

a = [1,2,3,[4,5,6]]
b = copy.copy(a)
a[3][0] = 5
print(a)
print(b)

a = [1,2,3,[4,5,6]]
b = copy.deepcopy(a)
a[3][0] = 5
print(a)
print(b)













