# /d /d+ /w(alpha numeric) /W(spaces and special char)
# /s, /t
# [] . ^ $ * + ? {} () \ |
#re.compile, re.M and re.I
#findall, match, search
#sub, replace

#match
import re

#r won't consider escape char
#^ is start of string
#$ end of string
#/d decimal

# \d,  \s | \S,  \w | \W | \w   trailing +

#pattern
j = re.compile(r'\w')

#input
k = "Hiandbye @ my age *is* 30we my birth day is 03 \t dec 1988 i.e 03-12-1988"
# # \d (digits)
# h = re.findall(r'\d', k)
# print(h)
# # \d+
# h = re.findall(r'\d+', k)
# print(h)
# # \s (space)
# h = re.findall(r'\s', k)
# print(h)
# # \t (tab space)
# h = re.findall(r'\t', k)
# print(h)
# # \w (consider only alpha and numeric)
# h = re.findall(r'\w', k)
# # print(h)
# # \w+ (consider only alpha and numeric)
# h = re.findall(r'\w+', k)
# print(h)
# # \W (consider spaces and special char)
# h = re.findall(r'\W', k)
# print(h)
# # \W (consider only special char, spaces)
# h = re.findall(r'\W+', k)
# print(h)

# [] . ^ $ * + ? {} () \ |
# # [] range [a-zA-Z0-9@_-\s\t\n]
# h = re.findall(r'[a-z\s]', k)
# print(h)

# # [^] is contridiction range [a-zA-Z0-9@_-\s\t\n]
# h = re.findall(r'[^a-z]', k)
# print(h)

# # . (select elements)(the number of dots will represent the group)
# # dont use + (it doesn't make sense)
# h = re.findall(r'...', k)
# print(h)

# g = "H03"
# # ^ $ (
# # dont use + (it doesn't make sense)
# h = re.findall(r'^[A-Z]\d+$', g)
# print(h)

# g = "H03 is fine"
# #* (dont use +)
# h = re.findall(r'^[A-Z]\d+.*$', g)
# print(h)














# # you need go with attribute (group, groups)
# # result is a list with all matched data
# h = re.match(j, k)
# print(dir(h))
# print(h.group())

# its a direct output
# result is a list with all matched data
# g = re.findall(j,k)
# g = re.findall(j,k)
# other way
# g = re.findall(r"\d+",k)
# print(g)
# g = re.findall(r"[A-Za-z0-9-]",k)
# print(g)
## all words
# g = re.findall(r"[A-Za-z0-9-]+",k)
# print(g)
# ## starts with
# g = re.findall(r"^[A-Za-z0-9-]+",k)
# print(g)

# [] vs ()
#
# g = re.findall(r"\w+",k)
# print(g)
# g = re.findall(r"([0-9]\w+)",k)
# print(g)
# #with spaces
# g = re.findall(r"\W[A-Za-z]+", k)
# print(g)
# # except those charactes
# g = re.findall(r"[^A-Z]+",k)
# print(g)
# # .
# g = re.findall(r"....",k)
# print(g)
# #* should never follow +
# g = re.findall(r"y*+",k)
# print(g)

# # re.M
# g = re.findall(r"^H.*$",k)
# print(g)

# * + ? {}
#* n number of repeations
#+ word
#? zero or n times

# k = "man"
# g = re.findall(r"m.n",k)
# print(g)

# #TODO
# k = "mzzzzzn"
# g = re.findall(r"m?zn",k)
# print(g)

# |

# g = re.findall(r"\w+ | \W+", k)
# print(g)

# #grouping
# #()
# print(k)
# g = re.findall(r'([0-9]{2}[a-zA-z]\w)\W([0-9a-zA-z]+)\W([0-9a-zA-z]+)', k)
# print(g)

# print (k)
# g = re.findall(r"\*(.*)\*",k)
# h = re.sub(r"\*(.*)\*", 'was', k)
# print(h)


print(k)
g = re.findall(r'([0-9]{2}[a-zA-z]\w)\W([0-9a-zA-z]+)\W([0-9a-zA-z]+)', k)
h = re.sub(r'([0-9]{2}[a-zA-z]\w)\W([0-9a-zA-z]+)\W([0-9a-zA-z]+)', 'was', k)
print(h)



