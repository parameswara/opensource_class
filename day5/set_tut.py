# k = [1,2,3,3,5]
# # Removes the duplicates
# k = set(k)
#
# print(k)
# #TODO
# #decorators
#
# #for loop
#
# for  k in range(10, 100):
#     print(k)
#
# #for loop with double outputs
# for index, k in enumerate(range(10, 100)):
#     print(index, k)

# # nested for
# index = 0
# for k in range(10, 100):
#     count_inc = 0
#     for _ in range(0, 90):
#         count_inc += 1
#         index += 1
#         print(index, k)
#         if count_inc:
#             break

# #continue
#
# for k in range(0,10):
#     print(k)
#
#     if k>5:
#         continue
#     print(k + 1)

# #pass
# for k in range(0,10):
#     pass

# #if
#
# if 1 :
#     print('passed')

# # all cond should be true for AND
# if 1 and 0:
#     print('passed')

# all cond should be true for AND
if 1 or 0 or (1 and 1):
    print('passed')