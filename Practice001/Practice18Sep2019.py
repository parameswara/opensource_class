# password = input("Enter a Passphrase: ")
# password = list(password)
# # print(password)
# # print(dir(password))
#
# def check_pass(password):
#     upper = False
#     lower = False
#     spechar = False
#     num = False
#
#     for each_ele in password:
#         if each_ele.isupper():
#             upper = True
#             break
#         else:
#             upper = False
#
#     for each_ele in password:
#         if each_ele.islower():
#             lower = True
#             break
#         else:
#             lower = False
#
#     # for each_ele in password:
#     #     # if each_ele in ['0','1','2','3','4','5','6','7','8','9']: # Implementation method 1 for checking numerical
#     #     if each_ele.isnumeric(): # Implementation method 2 for checking numerical
#     #         num = True
#     #         break
#     #     else:
#     #         num = False
#
#     for each_ele in password: # Implementation method 2 for checking numerical using try except blocks
#         try:
#             if int(each_ele) in range(0,10):
#                 num = True
#                 break
#             else:
#                 num = False
#         except ValueError:
#             pass
#
#     for each_ele in password:
#         if each_ele in ['@', '_', '*']:
#             spechar = True
#             break
#         else:
#             spechar = False
#
#     # print (upper , lower, spechar, num)
#
#     if upper and lower and spechar and num:
#         return ("Valid Password")
#     else:
#         return ("Invalid Password")
#
#
# validity = check_pass(password)
# print(validity)
############################# OPTIMISED CODE ################################################

password = input("Enter a Passphrase: ")

length = False
if len(password) >= 8:
    length = True

password = list(password)


# print(password)
# print(dir(password))
# print(length)


def check_pass(password, length):
    """

    Args:
        password(str):
        length:

    Returns:

    """
    upper = False
    lower = False
    spechar = False
    num = False
    if length:
        pass

    for each_ele in password:
        if each_ele.isupper() and upper == False:
            upper = True

        if each_ele.islower() and lower == False:
            lower = True

        try:
            if int(each_ele) in range(0, 10) and num == False:
                num = True
        except ValueError:
            pass

        if each_ele in ['@', '_', '*'] and spechar == False:
            spechar = True

    # print (length, upper , lower, num, spechar)

    if length and upper and lower and spechar and num:
        return "Valid Password"

    else:
        len_r, up_r, low_r, sp_r, num_r = "", "", "", "", ""

        if not length:
            len_r = "Length is less than 8 characters\n"

        if not upper:
            up_r = "At least one upper character is required\n"

        if not lower:
            low_r = "At least one lower character is required\n"

        if not spechar:
            sp_r = "At least one special character is required\n"

        if not num:
            num_r = "At least one number is required\n"

        return "Invalid Password \n" + len_r + up_r + low_r + sp_r + num_r


validity = check_pass(password, length)
print(validity)
