#assertEqual
#assertRaise
# debugger
# unix statements

#static vs class method

#unit testing  #mocking

# import unittest
#
# # def add(inp1, inp2):
# #     return sum([inp1, inp2, 1])
#
# def add(inp1, inp2):
#     raise ValueError
#
# class test_fun(unittest.TestCase):
#     def test_add(self):
#         with self.assertRaises(ValueError):
#             add(2,4)
#         # self.assertEqual(data, 5)
#
# k = test_fun()
# k.test_add()

#debugging

# enter or n will execute next line
# q will quit
# c will continue

# def add(inp1):
#     import pdb
#     pdb.set_trace()
#     f1 = inp1 + 1
#     f2 = f1 + 3
#     f3 = f2 + f1 + 5
#     return f3
#
# data = add(1)
# print(data)


#static vs class

class example():
    def __init__(self):
        self.name = "kuna"


    def pavan(self):
        print(self.name)
        self.name = "rao"
        print(self.name)
        self.one_more()

    @classmethod
    def another_pavan(cls):
        cls.name = "rao2"
        cls.pavan(cls)

    @staticmethod
    def one_more():
        name = "rao_one_more"
        print(name)

# def one_more():
#     name = "rao_one_more"
#     print(name)

h = example()
h.pavan()
# h.another_pavan()