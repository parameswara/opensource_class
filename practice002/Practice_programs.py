# # Question 1:
# # Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5,
# # between 2000 and 3200 (both included).
# # The numbers obtained should be printed in a comma-separated sequence on a single line.

result = []

for each in range(2000, 3201):
    if not each % 7 and each % 5:
        result.append(str(each))

print(', '.join(result))

## .join is and iterable String based function used to join
# # Python program to demonstrate the
# # use of join function to join list
# # elements with a character.
#
# list1 = ['1', '2', '3', '4']
#
# s = "-"
#
# # joins elements of list1 by '-'
# # and stores in sting s
# s = s.join(list1)
#
# # join use to join a list of
# # strings to a separator s
# print(s)
# Output is 1-2-3-4

############################################################################

# Question 2
# Level 1
#
# Question:
# Write a program which can compute the factorial of a given numbers.
# The results should be printed in a comma-separated sequence on a single line.
# Suppose the following input is supplied to the program:
# 8
# Then, the output should be:
# 40320

k = int(input("Enter a number to compute it's factorial: "))
l = k
fact = 1
while k > 1:
    fact = k*fact
    k = k-1

print("The factorial of ", l, " is ", fact)
############################
# method 2

def fact(x):
    if x == 0:
        return 1
    return x * fact(x - 1)


x = int(input("Enter a number to compute it's factorial: "))
print("The factorial of ",x, " is ", fact(x))
