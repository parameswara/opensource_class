# class based password validation

import os
data = os.environ
username = data["USERNAME"]
print(username)
k = {"panksing" : "Python@", "E7240":"Python@"}


import unittest

class passwordval():
    def __init__(self,password):
        self.password = password

    def verify(self):
       if self.password == k[username]:
           return username
       else:
           raise ValueError("did not match")


    def upper_val(self):
        for each in self.password:

            if each.isupper():
                return True
            else:
                raise ValueError("no upper case")

    def sarat(self, username):
        k=open('file.html','w')
        k.write("welcome {}".format(username))


    def lower_val(self):
        for each in self.password:
            if each.islower():
                return True
            #Sarat


    def special_val(self):
        #aravind
        spechar = False
        password_temp = list(self.password)
        for each in password_temp:
            if each in ['@', '_', '*'] and spechar == False:
                spechar = True
        if spechar:
            return spechar
        else:
            raise ValueError("At least one special character amongst @ , _ , * is required.")


validation = passwordval("Python@")
validation.upper_val()
validation.lower_val()
validation.special_val()
username = validation.verify()
validation.sarat(username)

class validation_test(unittest.TestCase):
    def val_upper_true(self):
        upper = passwordval("Kuna")
        result = upper.upper_val()
        self.assertEqual(result, True)

    def val_upper_error(self):
        upper = passwordval("kuna")
        with self.assertRaises(ValueError):
            upper.upper_val()

    def val_spechar_true(self):
        spechar1 = passwordval("@")
        spechar2 = passwordval("_")
        spechar3 = passwordval("*")
        result1 = spechar1.special_val()
        result2 = spechar2.special_val()
        result3 = spechar3.special_val()
        self.assertEqual(result1, True)
        self.assertEqual(result2, True)
        self.assertEqual(result3, True)

    def val_spechar_error(self):
        spechar1 = passwordval("random")
        with self.assertRaises(ValueError):
            spechar1.special_val()





test = validation_test()
test.val_upper_true()
test.val_upper_error()


