from csv_read import main as csv_main
from db_read import main as db_main
from json_read import DataReader
from xml_read import main as xml_main

k = DataReader("password_data.json")
dict3 = k()
top_dict = {}


def main():
    all_dict = [csv_main, db_main, k,  xml_main]
    for each_fun in all_dict:
        top_dict.update(each_fun())
    return top_dict

# def main():
#     dict1 = csv_main()
#     dict2 = db_main()
#     dict3 = k()
#     dict4 = xml_main()
#     top_dict = {}
#     top_dict.update(dict1)
#     top_dict.update(dict2)
#     top_dict.update(dict3)
#     top_dict.update(dict4)
#     return top_dict
#
#
if __name__ == "__main__":
    print(main())