import xml.etree.ElementTree as ET


def main():
    tree = ET.parse('password_data.xml')
    root = tree.getroot()
    lst1 = []
    lst2 = []
    dictionary = {}
    # print(dir(lst1))
    for key in root:
        # print(key.attrib)
        temp = key.attrib
        lst1.append(temp["name"])

        for each_data in key:
            # print(each_data.text)
            lst2.append(each_data.text)

    # print(lst1)
    # print(lst2)
    dictionary.update(zip(lst1,lst2))
    return dictionary

if __name__=="__main__":
    print(main())