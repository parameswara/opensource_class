import sqlite3

def main():
    k = sqlite3.Connection("password_data.db")
    cur = k.cursor()
    cur.execute("select username, password from users;")
    g = cur.fetchall()
    di = map(lambda d: dict(d),[g])
    for each in di:
        return each

if __name__=="__main__":
    print(main())
