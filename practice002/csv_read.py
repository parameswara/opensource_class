import csv
result = {}

def main():
    with open("password_data.csv") as csv_data:
        data = csv.reader(csv_data)
        next(data)
        for k in data:
            result[k[0]] = k[1]
    return result

if __name__ == "__main__":
    print(main())

