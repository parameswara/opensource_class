import json

class DataReader:
    def __init__(self, filename):
        self.sourcefile = filename

    def __call__(self):
        with open(self.sourcefile, "r") as read_file:
            data = json.load(read_file)
            return data

if __name__ == "__main__":
    k=DataReader("password_data.json")
    print(k())

