#!/bin/bash
wget http://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
tar -xvf Python-3.5.0.tgz
cd Python-3.5.0
mkdir -p /opt/python3
./configure --prefix=/opt/python3
make
make install
cd /opt/python3/bin/
./pip3 install pysqlite3

