# #reading text file
# # TODO: context manager
# # truncate for memory management
# k = open('data.txt', 'r')
# lines = k.readlines()
# print(lines)
# # lines = k.read()
# # print(lines)
#
# # removing trailing slash
# for each_element in lines:
#     print(each_element.rstrip())

# # Write a program which accepts a sequence of comma-separated numbers from console and generate a list and a tuple which contains every number.
# # Suppose the following input is supplied to the program:
# # 34,67,55,33,12,98
# # Then, the output should be:
# # ['34', '67', '55', '33', '12', '98']
# # ('34', '67', '55', '33', '12', '98')
#
# # k = input()
# # k_kuna = k.split(' ')
# # print(k_kuna)
#
# # k = [1,[2,[3,[4,5]]]]
# # output = [1,2,3,4,5]
#
# k = [1,[2,[3,[4,5]]]]
# collect = []
#
#
# def elements2(inp2):
#     for each_element in inp2:
#         if isinstance(each_element, list):
#             elements2(each_element)
#         else:
#            collect.append(each_element)
#
#
# def elements(inp):
#     result = elements2(inp)
#     print(result)
#
# elements(k)
# print(collect)
#
#
#
# # With a given integral number n, write a program to generate a dictionary that contains (i, i*i) such that is an integral number between 1 and n (both included). and then the program should print the dictionary.
# # Suppose the following input is supplied to the program:
# # 8
# # Then, the output should be:
# # {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}

# k = 5
# k = list(range(1, int(k)))
# result = {}
# for each_element in k:
#     k2 = dict([(each_element, each_element**2)])
#     result.update(k2)
#
# print(result)


# yield

def yield_tut():
    k = [1,2,3,4]
    for each in k:
        yield each


k = yield_tut()
print(next(k))
print(next(k))


