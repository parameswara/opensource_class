# functions / methods



##def <fun name>(<arg>):
##    <statements>
##    return

### error raises during  runtime only
##def addition():
##    return 'p' + 'k'
##
##print(addition())

##def inp_func(arg1):
##    return arg1 + ' ' + 'is input'
##
##print(inp_func('first value'))


### add two values taking from the user
##
##def add_from_user(var1, var2):
##    sum_var = int(var1) + int(var2)
##    return sum_var
##
##first_input = input('Enter a num value \n')
##second_input = input('Enter second num value \n')
##result = add_from_user(var1=first_input, var2=second_input)
##print(result)

### dynamic arguments
##def multiple_args(*args):
##    result = sum(args)
##    print(result)
##
##multiple_args(1, 2, 3)

### dynamic keyword arguments
##
##def multiple_key_word_args(**args):
##    print(args)
##
###result will be None as function doesn't resturn values
##result = multiple_key_word_args(j=1, h=2, j=3)

### local vs global
##def variables():
##    k = 'hi'
##    return
### raises error
##print(k)
##
### global
##def variables():
##    global k
##    k = 'hi'
##    return
##
##variables()
##print(k)

##k = 'hi'
##
##def variables():
##    k = 'bye'
##    return k
##
##print(variables())
##print(k)










