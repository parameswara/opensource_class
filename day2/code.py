#types
#strings are immutable
k = "strings.txt"

#type, isinstance

print(isinstance(k, str))

#dir
# arguments are given in paranthesis
s1 = k.replace('.txt', '.py')
s2 = k.zfill(20)
print(s1)
print(s2)
print(k)

#without escape
esc_k = "ssd\nwerr"
print('escape char exists', esc_k)

#using escape
esc2_k = "ssd\\nwerr"
print('escape char exists', esc2_k)

words = k.split('.')
print(words)

#slice

#index 
print (k[0])

print(k[:])
# reverse
print(k[::-1])
print(k)

#data types

# string
# list
# tuple
# dict


# similar to  array
# list #mutable
k = [1,2,3]
print(k)

# tuple #immutable
k = (1,2,3)
print(k)

# dict 
k = {"key":"value"}
print(k)





