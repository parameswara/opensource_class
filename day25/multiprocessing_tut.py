import time

def time_tut(arg):
    if arg == "Kuna":
        time.sleep(10)
        return arg
    elif arg == "pankaj":
        time.sleep(30)
        return arg
    elif arg == "aravind":
        time.sleep(2)
        return arg
    elif arg == "sarat":
        time.sleep(0)
        return arg

from multiprocessing import Pool
#cpu_count

def main():
    k = Pool(4 * 64)
    j = k.imap_unordered(time_tut, ["Kuna", "pankaj", "aravind", "sarat"])
    for each in j:
        print(each)


if __name__ == '__main__':
    main()
